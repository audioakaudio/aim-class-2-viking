﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FMOD.Studio;
using FMODUnity;



public class leftfoot_script : MonoBehaviour
{


    [FMODUnity.EventRef]
    public string FootstepsEvent;

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider Col)
    {
        if(Col.CompareTag("snow"))
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsEvent);
            FootstepsWalk.setParameterByName("surface", 0f);
            FootstepsWalk.set3DAttributes(transform.position.To3DAttributes());
            FootstepsWalk.start();
        }

        if (Col.CompareTag("Wood"))
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsEvent);
            FootstepsWalk.setParameterByName("surface", 2f);
            FootstepsWalk.set3DAttributes(transform.position.To3DAttributes());
            FootstepsWalk.start();
        }

        if (Col.CompareTag("rock"))
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsEvent);
            FootstepsWalk.setParameterByName("surface", 3f);
            FootstepsWalk.set3DAttributes(transform.position.To3DAttributes());
            FootstepsWalk.start();
        }
        if (Col.CompareTag("Water"))
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsEvent);
            FootstepsWalk.setParameterByName("surface", 7f);
            FootstepsWalk.set3DAttributes(transform.position.To3DAttributes());
            FootstepsWalk.start();
        }

        if (Col.CompareTag("near_water"))
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsEvent);
            FootstepsWalk.setParameterByName("surface", 6f);
            FootstepsWalk.set3DAttributes(transform.position.To3DAttributes());
            FootstepsWalk.start();
        }
        if (Col.CompareTag("crunchy_snow"))
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsEvent);
            FootstepsWalk.setParameterByName("surface", 1f);
            FootstepsWalk.set3DAttributes(transform.position.To3DAttributes());
            FootstepsWalk.start();
        }
    }

}
