﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class speed_controller : MonoBehaviour
{

    public float globalparameter;

    public vThirdPersonInput tpInput;
    // Start is called before the first frame update
    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();  
    }

    // Update is called once per frame
    void Update()
    {
        globalparameter = tpInput.cc.speed;
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("speed", globalparameter);
    }
}
