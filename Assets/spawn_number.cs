﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn_number : MonoBehaviour
{
    public GameObject spawnmanager; 
    public spawn_manager spawner;
    public int localNumber;
    
    // Start is called before the first frame update
    void Start()
    {
        spawnmanager = GameObject.FindGameObjectWithTag("spawnmanager");
        spawner = spawnmanager.GetComponent<spawn_manager>();

        
    }

    // Update is called once per frame
    public void decreaseNumberOfSpawns()
    {
        localNumber = spawner.numberOfenemies;
        localNumber = spawner.numberOfenemies - 1;
        spawner.numberOfenemies = localNumber;
        print("dead!");
    }
}
