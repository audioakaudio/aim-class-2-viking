﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;
using FMODUnity;

public class sphere_trigger : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string FootstepsEvent;

    private void OnTriggerEnter(Collider Col)
    {
        if (Col.CompareTag("Torso"))
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsEvent);

            FootstepsWalk.set3DAttributes(transform.position.To3DAttributes());
            FootstepsWalk.start();
            print("cloth");
        }
    }
}