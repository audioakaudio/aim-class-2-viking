﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FMOD.Studio;
using FMODUnity;

public class footsteps : MonoBehaviour
{

 

    [FMODUnity.EventRef]
    public string JumpEvent;

    [FMODUnity.EventRef]
    public string LandingEvent;

    public string _surfaceType;    
    private float _trackTime;

    public Transform positionSound;

   






    void jump_landing()
    {

        if (_surfaceType == "Wood")
        {
            FMOD.Studio.EventInstance Landing = FMODUnity.RuntimeManager.CreateInstance(LandingEvent);
            Landing.setParameterByName("surface", 1f);
            Landing.set3DAttributes(positionSound.position.To3DAttributes());
            Landing.start();
        }

        if (_surfaceType == "Water")
        {
            FMOD.Studio.EventInstance Landing = FMODUnity.RuntimeManager.CreateInstance(LandingEvent);
            Landing.setParameterByName("surface", 5f);
            Landing.set3DAttributes(positionSound.position.To3DAttributes());
            Landing.start();
        }

        if (_surfaceType == "near_water")
        {
            FMOD.Studio.EventInstance Landing = FMODUnity.RuntimeManager.CreateInstance(LandingEvent);
            Landing.setParameterByName("surface", 4f);
            Landing.set3DAttributes(positionSound.position.To3DAttributes());
            Landing.start();
        }

        if (_surfaceType == "snow")
        {
            FMOD.Studio.EventInstance Landing = FMODUnity.RuntimeManager.CreateInstance(LandingEvent);
            Landing.setParameterByName("surface", 0f);
            Landing.set3DAttributes(positionSound.position.To3DAttributes());
            Landing.start();
        }
        if (_surfaceType == "rock")
        {
            FMOD.Studio.EventInstance Landing = FMODUnity.RuntimeManager.CreateInstance(LandingEvent);
            Landing.setParameterByName("surface", 2f);
            Landing.set3DAttributes(positionSound.position.To3DAttributes());
            Landing.start();
        }


    }

    void jump()
    {


        FMOD.Studio.EventInstance Jumping = FMODUnity.RuntimeManager.CreateInstance(JumpEvent);
        Jumping.set3DAttributes(positionSound.position.To3DAttributes());
        Jumping.start();

    }


    private void Update() 
    {     
    if (Time.time - _trackTime < 0.1f) return;
        RaycastHit hit; 
         if (Physics.Raycast(positionSound.position + transform.up, -transform.up, out hit, 1.2f)) 
         {
             _surfaceType = hit.collider.tag; 
        print(hit.collider.tag);  
         }
        _trackTime = Time.time;
      }
}
