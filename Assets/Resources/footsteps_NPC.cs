﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FMOD.Studio;
using FMODUnity;

public class footsteps_NPC : MonoBehaviour
{
   
    [FMODUnity.EventRef]
    public string FootstepsWalkEvent;

    [FMODUnity.EventRef]
    public string JumpEvent;

    [FMODUnity.EventRef]
    public string LandingEvent;

    public string _surfaceType;
    private float _trackTime;

    public Transform positionNPCSound;

   


    void footstep_walk()

    {

        if (_surfaceType == "Water")
        {

            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsWalkEvent);
            FootstepsWalk.setParameterByName("surface", 7f);
            FootstepsWalk.set3DAttributes(positionNPCSound.position.To3DAttributes());
            FootstepsWalk.start();
        }

        if (_surfaceType == "snow")
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsWalkEvent);
            FootstepsWalk.setParameterByName("surface", 0f);
            FootstepsWalk.set3DAttributes(positionNPCSound.position.To3DAttributes());
            FootstepsWalk.start();
        }

        if (_surfaceType == "near_water")
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsWalkEvent);
            FootstepsWalk.setParameterByName("surface", 6f);
            FootstepsWalk.set3DAttributes(positionNPCSound.position.To3DAttributes());
            FootstepsWalk.start();
        }

        if (_surfaceType == "Wood")
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsWalkEvent);
            FootstepsWalk.setParameterByName("surface", 2f);
            FootstepsWalk.set3DAttributes(positionNPCSound.position.To3DAttributes());
            FootstepsWalk.start();
        }

        if (_surfaceType == "crunchy_snow")
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsWalkEvent);
            FootstepsWalk.setParameterByName("surface", 1f);
            FootstepsWalk.set3DAttributes(positionNPCSound.position.To3DAttributes());
            FootstepsWalk.start();
        }

        if (_surfaceType == "rock")
        {
            FMOD.Studio.EventInstance FootstepsWalk = FMODUnity.RuntimeManager.CreateInstance(FootstepsWalkEvent);
            FootstepsWalk.setParameterByName("surface", 3f);
            FootstepsWalk.set3DAttributes(positionNPCSound.position.To3DAttributes());
            FootstepsWalk.start();
        }



    }






    void jump_landing()
    {

        if (_surfaceType == "Wood")
        {
            FMOD.Studio.EventInstance Landing = FMODUnity.RuntimeManager.CreateInstance(LandingEvent);
            Landing.setParameterByName("surface", 1f);
            Landing.set3DAttributes(positionNPCSound.position.To3DAttributes());
            Landing.start();
        }

        if (_surfaceType == "Water")
        {
            FMOD.Studio.EventInstance Landing = FMODUnity.RuntimeManager.CreateInstance(LandingEvent);
            Landing.setParameterByName("surface", 5f);
            Landing.set3DAttributes(positionNPCSound.position.To3DAttributes());
            Landing.start();
        }

        if (_surfaceType == "near_water")
        {
            FMOD.Studio.EventInstance Landing = FMODUnity.RuntimeManager.CreateInstance(LandingEvent);
            Landing.setParameterByName("surface", 4f);
            Landing.set3DAttributes(positionNPCSound.position.To3DAttributes());
            Landing.start();
        }

        if (_surfaceType == "snow")
        {
            FMOD.Studio.EventInstance Landing = FMODUnity.RuntimeManager.CreateInstance(LandingEvent);
            Landing.setParameterByName("surface", 0f);
            Landing.set3DAttributes(positionNPCSound.position.To3DAttributes());
            Landing.start();
        }
        if (_surfaceType == "rock")
        {
            FMOD.Studio.EventInstance Landing = FMODUnity.RuntimeManager.CreateInstance(LandingEvent);
            Landing.setParameterByName("surface", 2f);
            Landing.set3DAttributes(positionNPCSound.position.To3DAttributes());
            Landing.start();
        }


    }

    void jump()
    {


        FMOD.Studio.EventInstance Jumping = FMODUnity.RuntimeManager.CreateInstance(JumpEvent);
        Jumping.set3DAttributes(positionNPCSound.position.To3DAttributes());
        Jumping.start();

    }


    private void Update()
    {
        if (Time.time - _trackTime < 0.1f) return;
        RaycastHit hit;
        if (Physics.Raycast(positionNPCSound.position + transform.up, -transform.up, out hit, 1.2f))
        {
            _surfaceType = hit.collider.tag;
            print(hit.collider.tag);
        }
        _trackTime = Time.time;
        print(positionNPCSound + "NPC");
    }
  
}
