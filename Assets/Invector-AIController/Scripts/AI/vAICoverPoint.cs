﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Invector.vCharacterController.AI
{
    [RequireComponent(typeof(BoxCollider))]
    [vClassHeader("AI Cover Point")]
    public class vAICoverPoint : vMonoBehaviour
    {
        private void Start()
        {
            if (boxCollider) boxCollider.isTrigger = true;         
        }
        public float posePositionZ = 0.5f;

        public BoxCollider boxCollider;

        public Vector3 posePosition
        {
            get
            {
                return transform.position + transform.forward * posePositionZ;
            }
        }

        public bool isOccuped;

      
    }
}